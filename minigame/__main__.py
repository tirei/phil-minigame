import gui

def main():
    size = 16
    objects = []
    difficulty = 1
    view = gui.GUI(size, objects, difficulty)
    view.createGUI()

if __name__ == "__main__":
    main()
